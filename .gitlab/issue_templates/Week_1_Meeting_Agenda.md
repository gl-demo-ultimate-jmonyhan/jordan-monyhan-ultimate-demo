## Agenda

- Kickoff!
- What do you hope to learn?
- Sneak Peek of What's to Come
- Reading Content - Questions and Feedback
- Exercise - Questions and Feedback

### Week 1 Specifics
* Discuss merge request settings
* Approve the MR
* Merge the MR

~meeting_agenda
